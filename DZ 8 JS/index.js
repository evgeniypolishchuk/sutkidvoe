let input = document.querySelector('input');
let list = document.querySelector('.price-list');
let alert = document.createElement("span")

function createPrice(event) {
    const inputValue = input.value;
    if(inputValue < 0 ){ 
        const alert = document.createElement("span")
        alert.innerText = 'Please enter correct price'
        alert.style.color = 'black'
        document.body.append(alert)   
        input.style.borderColor = 'black'
    }else if (event.key === 'Enter' && inputValue.length) {
        let listElement = document.createElement('li');
        listElement.classList.add('item');
        listElement.innerText = `Текущая цена: ${inputValue}$`;
        input.value = '';
        input.style.borderColor = 'purple'

        const deleteButton = document.createElement('button');
        deleteButton.innerText = 'X';
        deleteButton.classList.add('deleteButton');

        list.append(listElement);
        listElement.append(deleteButton);
        
    }

}


input.addEventListener('focus' , (e) => {
    input.style.border = "5px solid purple"
});
input.addEventListener('blur' , () => {
    input.style.border = "1px solid black"
});
input.addEventListener('keydown', createPrice);

list.addEventListener('click', event => {
    if (event.target.tagName === 'BUTTON' && event.target.classList.contains('deleteButton')) {
        event.target.parentNode.remove();
    }
})
